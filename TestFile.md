{::options parse_block_html="true" /}

Test Alert-Info Box
{: .alert .alert-info}

{::options parse_block_html="false" /}

> Noch keine Lösung dafür gefunden. Ist wahrscheinlich zu aufwendig für OER

# TestFile

### Backgorund coloring:

<div class="panel panel-info">

Sample Info-Panel

</div>

`only in one line possible`

```
multi
lines
```

[+green=correct+]
[-red=false-]

### Checkbox:

<p>Check the correct statements.</p>

<div>
  <input type="checkbox" id="statement1" name="statement1">
  <label for="scales">1+2=3</label>
</div>

<div>
  <input type="checkbox" id="statement2" name="statement2">
  <label for="horns">2+2=4</label>
</div>

### Collapse:

<details>
  <summary markdown="span">Solution down here.</summary>

    Great, you found the solution!
    
</details>

### Video:

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/enMumwvLAug" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

### Table:

|    Features     | GitLab       | GitHub          | Moodle         |
|-----------------|--------------|-----------------|----------------|
| Tables          | check        |                 |                |
| Collapse        |              |                 |                |
| ...             |              |                 |                |

#### Code-Blocks:

**In-line:** 
`Example` code block in-line.

**Fenced:**
```python
def hello
    print("Hello World!")
end
```

### Links:

Sample [Link](https://www.youtube.com/embed/enMumwvLAug)

### User Input:

<label for="uinput">Please enter something.</label>
<input type="text" id="uinput" name="uinput">
